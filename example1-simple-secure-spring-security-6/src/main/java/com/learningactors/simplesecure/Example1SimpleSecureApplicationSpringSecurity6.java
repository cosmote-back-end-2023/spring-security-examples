package com.learningactors.simplesecure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Example1SimpleSecureApplicationSpringSecurity6 {

  public static void main(String[] args) {
    SpringApplication.run(Example1SimpleSecureApplicationSpringSecurity6.class, args);
  }

}
