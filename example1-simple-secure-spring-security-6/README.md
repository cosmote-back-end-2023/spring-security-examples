# Example 1

## Spring Security 6 example

This example is only providing a minimal Spring Security 6 configuration class example, which is basically a copy of 
default Security configuration found in `SpringBootWebSecurityConfiguration`

We can also comment out SecurityConfig completely, in order to experience that Spring security secures **all** endpoints
without any configuration by default.

---
The test user is included in application properties
If we don't configure any test user and there are no other users provided
from AuthenticationProvider then a default test one is logged into application logs, upon startup.


### src/main/resources/application.properties
```properties
spring.security.user.name=test
spring.security.user.password=1234
```



