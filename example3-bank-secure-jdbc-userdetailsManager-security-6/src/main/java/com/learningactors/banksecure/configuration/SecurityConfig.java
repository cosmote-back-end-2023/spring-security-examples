package com.learningactors.banksecure.configuration;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.authorizeHttpRequests(configurer ->
                configurer
                        .requestMatchers(
                                AntPathRequestMatcher.antMatcher("/balance"),
                                AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/loans"))
                        .authenticated()

                        .requestMatchers(AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/contact"))
                        .permitAll()

                        //.requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll()
                        .requestMatchers(PathRequest.toH2Console()).permitAll()
                        .anyRequest().authenticated()
        ).logout(
                configurer -> configurer.logoutUrl("/logout").permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID")
        );

        //We need to enable httpBasic when formLogin is enabled, so that REST apis can authenticate bearing a basic auth token
        http.formLogin(Customizer.withDefaults());
        http.httpBasic(Customizer.withDefaults());

        //Needed for h2-console frames
        http.headers(headers -> headers.frameOptions(frameOptionsConfig -> frameOptionsConfig.disable()));

        http.csrf(csrf -> csrf.disable());

        return http.build();
    }

  @Bean
  public UserDetailsManager userDetailsManager(DataSource dataSource) {
    return new JdbcUserDetailsManager(dataSource);
  }


  @Bean
  PasswordEncoder passwordEncoder() {
    return NoOpPasswordEncoder.getInstance();
  }

    //the above configuration is including specfic mvc matching
    //due to https://github.com/spring-projects/spring-security/issues/13568#issuecomment-1712754300
    //normally the following would be sufficient
    /*
        http.authorizeHttpRequests(configurer ->
                configurer
                        .requestMatchers("/balance", "/loans").authenticated()
                        .requestMatchers("/contact", "/h2-console/**").permitAll()
                        .anyRequest().authenticated()
        ).logout(
                configurer -> configurer.logoutUrl("/logout").permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID")
        );
    */

}



