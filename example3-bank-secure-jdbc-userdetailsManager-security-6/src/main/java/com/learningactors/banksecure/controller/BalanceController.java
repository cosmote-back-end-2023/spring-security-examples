package com.learningactors.banksecure.controller;

import com.learningactors.banksecure.model.Balance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class BalanceController {

  @GetMapping("/balance")
  public Balance getMyBalance() {
    return new Balance(BigDecimal.TEN, "Euro", BigDecimal.ONE);
  }
}
