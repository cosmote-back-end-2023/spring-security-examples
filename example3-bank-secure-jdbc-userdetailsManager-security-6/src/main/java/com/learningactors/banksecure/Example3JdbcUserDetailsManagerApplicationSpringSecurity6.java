package com.learningactors.banksecure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Example3JdbcUserDetailsManagerApplicationSpringSecurity6 {

  public static void main(String[] args) {
    SpringApplication.run(Example3JdbcUserDetailsManagerApplicationSpringSecurity6.class, args);
  }

}
