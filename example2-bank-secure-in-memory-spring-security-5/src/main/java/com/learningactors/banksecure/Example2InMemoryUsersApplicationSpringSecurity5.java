package com.learningactors.banksecure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Example2InMemoryUsersApplicationSpringSecurity5 {

  public static void main(String[] args) {
    SpringApplication.run(Example2InMemoryUsersApplicationSpringSecurity5.class, args);
  }

}
