package com.learningactors.banksecure.controller;

import com.learningactors.banksecure.model.Contact;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContactUsController {

  @PostMapping("/contact")
  public Contact saveContactInquiryDetails(@RequestBody Contact contact) {
    return contact;
  }

  @GetMapping("/contact")
  public List<Contact> getContacts() {
    return List.of(new Contact("1","christos","chris@peris.com","subject", "message"));
  }
}
