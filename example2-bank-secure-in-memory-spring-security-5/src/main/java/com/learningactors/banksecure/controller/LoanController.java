package com.learningactors.banksecure.controller;

import com.learningactors.banksecure.model.Loan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LoanController {

  @GetMapping("/loans")
  public List<Loan> getLoanDetails() {
    return List.of(new Loan(1,1,"Home renovations", 50, 20));
  }

}
