package com.learningactors.banksecure.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration//public class SecurityConfig extends WebSecurityConfigurerAdapter {
public class SecurityConfig {

  @Bean
  public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
    http.authorizeRequests()
            .antMatchers("/balance", "/loans").authenticated()
            .antMatchers("/contact").permitAll()
                    .and().logout().permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID");

    http.formLogin();
    http.httpBasic();
    http.cors().disable();
    http.csrf().disable();
    http.headers().frameOptions().disable();
    return http.build();
  }

  @Bean
  public InMemoryUserDetailsManager userDetailsManager() {

    UserDetails admin = User.builder()
            .username("admin")
            .password("12345")
            .roles("ADMIN")
            .build();

    UserDetails user = User.builder()
            .username("user")
            .password("12345")
            .roles("USER")
            .build();

    return new InMemoryUserDetailsManager(user, admin);
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return NoOpPasswordEncoder.getInstance();
  }
}



