package com.learningactors.banksecure.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact_messages")
public class Contact {

  @Id
  private String contactId;
  private String contactName;
  private String contactEmail;
  private String subject;
  private String message;


}
