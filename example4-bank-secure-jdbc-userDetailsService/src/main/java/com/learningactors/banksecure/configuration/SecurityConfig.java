package com.learningactors.banksecure.configuration;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        //needed due to Spring's Security 6 migration and request caching and quering approach:
        //https://docs.spring.io/spring-security/reference/5.8/migration/servlet/session-management.html#requestcache-query-optimization
        HttpSessionRequestCache requestCache = new HttpSessionRequestCache();
        requestCache.setMatchingRequestParameterName(null);

        http.authorizeHttpRequests(configurer ->
                        configurer
                                .requestMatchers(
                                        AntPathRequestMatcher.antMatcher("/balance"),
                                        AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/loans"))
                                .authenticated()

                                .requestMatchers(AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/contact"),
                                        //so that errorDetails are added to the response
                                        AntPathRequestMatcher.antMatcher("/error"))
                                .permitAll()

                                //.requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll()
                                .requestMatchers(PathRequest.toH2Console()).permitAll()
                                .anyRequest().authenticated()
                ).requestCache((cache) -> cache
                        .requestCache(requestCache))
                .logout(
                        configurer -> configurer.logoutUrl("/logout").permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID")
                );

        //We need to enable httpBasic when formLogin is enabled, so that REST apis can authenticate bearing a basic auth token
        http.formLogin(Customizer.withDefaults());
        http.httpBasic(Customizer.withDefaults());

        //Needed for h2-console frames
        http.headers(headers -> headers.frameOptions(frameOptionsConfig -> frameOptionsConfig.disable()));

        http.csrf(csrf -> csrf.disable());

        return http.build();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}




