package com.learningactors.simplesecure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Example1SimpleSecureApplicationSpringSecurity5 {

  public static void main(String[] args) {
    SpringApplication.run(Example1SimpleSecureApplicationSpringSecurity5.class, args);
  }

}
