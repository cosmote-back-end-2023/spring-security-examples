package com.learningactors.example8_jwt.controller;


import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@AllArgsConstructor
public class LoginController {
  private UserDetailsService userDetailsService;

  @PostMapping("/my-login")
  public UserDetails getUserDetails(Principal principal) {
    return this.userDetailsService.loadUserByUsername(principal.getName());
  }
}
