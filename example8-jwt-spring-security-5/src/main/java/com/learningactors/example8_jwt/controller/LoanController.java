package com.learningactors.example8_jwt.controller;

import com.learningactors.example8_jwt.model.Loan;
import com.learningactors.example8_jwt.service.LoanService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class LoanController {
  private final LoanService loanService;

  @GetMapping("/loans")
  public List<Loan> getLoanDetails() {
    return this.loanService.findLoans();
  }

}
