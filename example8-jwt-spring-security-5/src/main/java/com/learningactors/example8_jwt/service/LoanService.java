package com.learningactors.example8_jwt.service;

import com.learningactors.example8_jwt.model.Loan;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoanService {


  public List<Loan> findLoansBiggerThan20() {
    return this.findLoans().stream().filter(l-> l.getTotalLoan() > 20).collect(Collectors.toList());
  }

  @PostFilter("filterObject.customerEmail == authentication.name")
  public List<Loan> findLoans() {
    return new ArrayList<>(List.of(
            new Loan(2, "admin@aa.aa", "dasda", 50, 40),
            new Loan(2, "aa@aa.aa", "dasda", 50, 40),
            new Loan(2, "aa@aa.aa", "dasda", 20, 40),
            new Loan(2, "bb@aa.aa", "dasda", 50, 40)));

  }
}
