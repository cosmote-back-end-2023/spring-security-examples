package com.learningactors.example8_jwt.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SecurityCustomer implements UserDetails {
  private final Customer customer;

  public static SecurityCustomer of(Customer customer) {
    return new SecurityCustomer(customer);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> authorities = new ArrayList<>();
    customer.getAuthorities()
            .stream()
            .map(auth -> new SimpleGrantedAuthority(auth.getName()))
            .forEach(authorities::add);

    return authorities;
  }

  @Override
  public String getPassword() {
    return customer.getPwd();
  }

  @Override
  public String getUsername() {
    return customer.getEmail();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

}
