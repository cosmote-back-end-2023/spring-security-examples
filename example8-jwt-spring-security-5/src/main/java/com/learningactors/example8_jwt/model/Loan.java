package com.learningactors.example8_jwt.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Loan {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int loanNumber;

  private String customerEmail;

  private String loanType;

  private int totalLoan;

  private int amountPaid;
}
