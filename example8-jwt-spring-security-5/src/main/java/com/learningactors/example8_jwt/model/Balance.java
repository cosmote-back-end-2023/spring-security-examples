package com.learningactors.example8_jwt.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Balance {

  private BigDecimal amount;
  private String currency;
  private BigDecimal reservedAmount;
}
