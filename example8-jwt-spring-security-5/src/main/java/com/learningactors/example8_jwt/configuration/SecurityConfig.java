package com.learningactors.example8_jwt.configuration;

import com.learningactors.example8_jwt.configuration.filter.JWTTokenGeneratorFilter;
import com.learningactors.example8_jwt.configuration.filter.JWTTokenValidatorFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration//public class SecurityConfig extends WebSecurityConfigurerAdapter {
@EnableWebSecurity(debug = true)
public class SecurityConfig {

  @Bean
  public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.authorizeRequests()
            .antMatchers("/balance", "/loans").access("hasRole('ADMIN') or hasRole('USER')")
            .mvcMatchers(HttpMethod.GET, "/contact").hasRole("ADMIN")
            .mvcMatchers(HttpMethod.POST, "/contact").permitAll()
            .mvcMatchers(HttpMethod.GET, "/home").authenticated()
            .antMatchers("/h2-console/**").permitAll()
            .mvcMatchers(HttpMethod.POST, "/login").permitAll()
            .and()
            .addFilterAfter(new JWTTokenGeneratorFilter(), BasicAuthenticationFilter.class)
            .addFilterBefore(new JWTTokenValidatorFilter(), BasicAuthenticationFilter.class);

    http.formLogin().disable();
    http.httpBasic();
    http.cors().disable();
    http.csrf().disable();
    http.headers().frameOptions().disable();
    return http.build();
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return NoOpPasswordEncoder.getInstance();
  }


}
