package com.learningactors.example8_jwt.configuration.filter;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SecurityConstants {

  public static final String JWT_KEY = "secret_key";
  public static final String JWT_HEADER = "Authorization";

  public static final long JWT_TOKEN_VALIDITY = 3 * 60 * 60;
}
