package com.learningactors.example8_jwt.repository;

import com.learningactors.example8_jwt.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
