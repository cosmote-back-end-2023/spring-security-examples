package com.learningactors.example8_jwt.repository;

import com.learningactors.example8_jwt.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

  List<Customer> findByEmail(String email);
}
