package com.learningactors.banksecure.service;

import com.learningactors.banksecure.model.Customer;
import com.learningactors.banksecure.model.SecurityCustomer;
import com.learningactors.banksecure.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class CustomerUserDetailsService implements UserDetailsService {

  private final CustomerRepository customerRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    List<Customer> customer = customerRepository.findByEmail(username);
    if (customer.size() != 1) {
      throw new UsernameNotFoundException("User details not found for the user : " + username);
    }
    return SecurityCustomer.of(customer.get(0));
  }
}
