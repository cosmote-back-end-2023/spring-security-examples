package com.learningactors.banksecure;

import com.learningactors.banksecure.model.Authority;
import com.learningactors.banksecure.model.Customer;
import com.learningactors.banksecure.repository.AuthorityRepository;
import com.learningactors.banksecure.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.HashSet;
import java.util.List;

@SpringBootApplication
@AllArgsConstructor
public class Example5BankSecureApplication implements CommandLineRunner {

  private final AuthorityRepository authorityRepository;
  private final CustomerRepository customerRepository;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  public static void main(String[] args) {
    SpringApplication.run(Example5BankSecureApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {

    Customer customer = new Customer(5, "aa@aa.aa", bCryptPasswordEncoder.encode("12345"), new HashSet<>());
    Customer customer2 = new Customer(6, "bb@aa.aa", bCryptPasswordEncoder.encode("12345"), new HashSet<>());
    Customer customer3 = new Customer(7, "cc@aa.aa", bCryptPasswordEncoder.encode("12345"), new HashSet<>());
    Customer customer4 = new Customer(8, "admin@aa.aa", bCryptPasswordEncoder.encode("12345"), new HashSet<>());
    var savedCustomer = this.customerRepository.save(customer);
    var savedCustomer2 = this.customerRepository.save(customer2);
    var savedCustomer3 =this.customerRepository.save(customer3);
    var savedCustomer4 = this.customerRepository.save(customer4);

    Authority user = new Authority(1L, "ROLE_USER", savedCustomer);
    Authority user2 = new Authority(2L, "ROLE_USER", savedCustomer2);
    Authority user3 = new Authority(3L, "ROLE_USER", savedCustomer3);
    Authority admin = new Authority(4L, "ROLE_ADMIN", savedCustomer4);
    Authority admin2 = new Authority(5L, "ROLE_USER", savedCustomer4);
    this.authorityRepository.saveAll(List.of(user, user2, user3, admin, admin2));

  }
}
