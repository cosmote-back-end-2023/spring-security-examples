package com.learningactors.banksecure.repository;

import com.learningactors.banksecure.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
