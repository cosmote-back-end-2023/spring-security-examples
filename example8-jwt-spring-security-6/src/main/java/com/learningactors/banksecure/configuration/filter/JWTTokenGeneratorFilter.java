package com.learningactors.banksecure.configuration.filter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.learningactors.banksecure.configuration.filter.SecurityConstants.JWT_TOKEN_VALIDITY;


public class JWTTokenGeneratorFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (null != authentication) {
      String jwt = Jwts.builder().setIssuer("Spring demo").setSubject("JWT Token")
              .claim("username", authentication.getName())
              .claim("authorities", populateAuthorities(authentication.getAuthorities()))
              .setIssuedAt(new Date())
              .setExpiration(new Date((new Date()).getTime() + JWT_TOKEN_VALIDITY * 1000))
              .setHeaderParam("typ", "JWT")
              .signWith(SignatureAlgorithm.HS512, SecurityConstants.JWT_KEY).compact();
      response.setHeader(SecurityConstants.JWT_HEADER, jwt);
    }
  }

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {
    return !request.getServletPath().equals("/my-login");
  }


  private String populateAuthorities(Collection<? extends GrantedAuthority> collection) {
    Set<String> authoritiesSet = new HashSet<>();
    for (GrantedAuthority authority : collection) {
      authoritiesSet.add(authority.getAuthority());
    }
    return String.join(",", authoritiesSet);
  }
}
