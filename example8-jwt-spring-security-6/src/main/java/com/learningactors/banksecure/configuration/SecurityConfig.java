package com.learningactors.banksecure.configuration;

import com.learningactors.banksecure.configuration.filter.AuthoritiesLoggingAfterFilter;
import com.learningactors.banksecure.configuration.filter.JWTTokenGeneratorFilter;
import com.learningactors.banksecure.configuration.filter.JWTTokenValidatorFilter;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.expression.WebExpressionAuthorizationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity(debug = true)
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        //needed due to Spring's Security 6 migration and request caching and quering approach:
        //https://docs.spring.io/spring-security/reference/5.8/migration/servlet/session-management.html#requestcache-query-optimization
        HttpSessionRequestCache requestCache = new HttpSessionRequestCache();
        requestCache.setMatchingRequestParameterName(null);

        http.authorizeHttpRequests(configurer ->
                        configurer
                                .requestMatchers(
                                        AntPathRequestMatcher.antMatcher("/balance"),
                                        AntPathRequestMatcher.antMatcher("/loans"))
                                .access(new WebExpressionAuthorizationManager("hasRole('ADMIN') or hasRole('USER')"))

                                .requestMatchers(
                                        AntPathRequestMatcher.antMatcher(HttpMethod.POST,"/contact"))
                                .hasRole("ADMIN")

                                .requestMatchers(AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/contact"),
                                        AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/home"),
                                        AntPathRequestMatcher.antMatcher(HttpMethod.POST, "/login"),
                                        //so that errorDetails are added to the response
                                        AntPathRequestMatcher.antMatcher("/error"))
                                .permitAll()


                                //.requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll()
                                .requestMatchers(PathRequest.toH2Console()).permitAll()
                                .anyRequest().authenticated()
                ).requestCache((cache) -> cache
                        .requestCache(requestCache))
                .logout(
                        configurer -> configurer.logoutUrl("/logout").permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID")
                ).sessionManagement((session) -> session
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                );

        http.addFilterAfter(new AuthoritiesLoggingAfterFilter(), BasicAuthenticationFilter.class);

        http.addFilterAfter(new JWTTokenGeneratorFilter(), BasicAuthenticationFilter.class);
        http.addFilterBefore(new JWTTokenValidatorFilter(), BasicAuthenticationFilter.class);

        http.csrf(csrf -> csrf.disable());
        http.cors(cors -> cors.disable());
        http.formLogin(formLogin -> formLogin.disable());
        //Needed for h2-console frames
        http.headers(headers -> headers.frameOptions(frameOptionsConfig -> frameOptionsConfig.disable()));

        http.httpBasic(Customizer.withDefaults());

        return http.build();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}




