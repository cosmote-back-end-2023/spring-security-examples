package com.learningactors.banksecure.controller;

import com.learningactors.banksecure.model.Loan;
import com.learningactors.banksecure.service.LoanService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class LoanController {
  private final LoanService loanService;

  @GetMapping("/loans")
  public List<Loan> getLoanDetails() {
    return loanService.findLoans();
  }

}
