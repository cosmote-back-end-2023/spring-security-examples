package com.learningactors.example6_oath2_github;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Example9Oath2GithubApplication {

  public static void main(String[] args) {
    SpringApplication.run(Example9Oath2GithubApplication.class, args);
  }

}
