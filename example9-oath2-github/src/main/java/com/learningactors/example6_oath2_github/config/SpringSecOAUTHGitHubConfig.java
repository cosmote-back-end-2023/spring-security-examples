package com.learningactors.example6_oath2_github.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SpringSecOAUTHGitHubConfig {

    @Bean
    protected SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().authenticated().and().oauth2Login();
        return http.build();
    }

/*  private ClientRegistration clientRegistration() {
    return
            CommonOAuth2Provider.GITHUB.getBuilder("github").clientId(
                            " Iv1.2dfc066444845dd5")
                    .clientSecret("7a1f5e3ee1fd8fa7f1ce40893c83d3be5d992219").build();
  }*/


    private ClientRegistration clientRegistration() {
        ClientRegistration cr =
                ClientRegistration.withRegistrationId("github").clientId(
                                "d07fe1c90c23819f10bc")
                        .clientSecret("9f04f0e65ecdebdddd663f0c330480c810b4b55c").scope(new String[]
                                {"read:user"})
                        .authorizationUri("https://github.com/login/oauth/authorize")
                        .tokenUri("https://github.com/login/oauth/access_token").userInfoUri(
                                "https://api.github.com/user")
                        .userNameAttributeName("id").clientName("GitHub")
                        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                        .redirectUriTemplate("{baseUrl}/{action}/oauth2/code/{registrationId}").build
                                ();
        return cr;
    }


    @Bean
    public ClientRegistrationRepository clientRepository() {
        ClientRegistration clientReg = clientRegistration();
        return new
                InMemoryClientRegistrationRepository(clientReg);
    }
}
