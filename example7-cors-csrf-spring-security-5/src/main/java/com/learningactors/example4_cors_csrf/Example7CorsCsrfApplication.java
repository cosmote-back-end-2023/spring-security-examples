package com.learningactors.example4_cors_csrf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Example7CorsCsrfApplication {

  public static void main(String[] args) {
    SpringApplication.run(Example7CorsCsrfApplication.class, args);
  }

}
