package com.learningactors.banksecure.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact_messages")
public class Contact {

  @Id
  private String contactId;
  private String contactName;
  private String contactEmail;
  private String subject;
  private String message;


}
