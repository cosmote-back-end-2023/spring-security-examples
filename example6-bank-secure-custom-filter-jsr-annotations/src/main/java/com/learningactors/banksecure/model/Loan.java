package com.learningactors.banksecure.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Loan {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int loanNumber;

  private String customerEmail;

  private String loanType;

  private int totalLoan;

  private int amountPaid;
}
