package com.learningactors.banksecure.service;

import com.learningactors.banksecure.model.Loan;
import jakarta.annotation.security.RolesAllowed;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoanService {


  public List<Loan> findLoansBiggerThan20() {
    return this.findLoans().stream().filter(l-> l.getTotalLoan() > 20).collect(Collectors.toList());
  }

  @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @PostFilter("filterObject.customerEmail == authentication.name")
  public List<Loan> findLoans() {

    return new ArrayList<>(List.of(
            new Loan(2, "admin@aa.aa", "dasda", 50, 40),
            new Loan(2, "aa@aa.aa", "dasda", 50, 40),
            new Loan(2, "aa@aa.aa", "dasda", 20, 40),
            new Loan(2, "bb@aa.aa", "dasda", 50, 40)));

  }

/*  @RolesAllowed("ROLE_USER")
  public Loan findLoandById(String id) {

    //var loan = this.repo.findById(id)
    if()
  }*/
}
