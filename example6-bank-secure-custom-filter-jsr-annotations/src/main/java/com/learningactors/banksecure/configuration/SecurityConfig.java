package com.learningactors.banksecure.configuration;

import com.learningactors.banksecure.configuration.filter.AuthoritiesLoggingAfterFilter;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.expression.WebExpressionAuthorizationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        //needed due to Spring's Security 6 migration and request caching and quering approach:
        //https://docs.spring.io/spring-security/reference/5.8/migration/servlet/session-management.html#requestcache-query-optimization
        HttpSessionRequestCache requestCache = new HttpSessionRequestCache();
        requestCache.setMatchingRequestParameterName(null);

        http.authorizeHttpRequests(configurer ->
                        configurer
                                //Following request matcher is implemented through jsr annotations in LoanService methods
                                /*
                                .requestMatchers(AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/loans"))
                                .access(new WebExpressionAuthorizationManager("hasRole('ADMIN') or hasRole('USER')"))*/

                                .requestMatchers(
                                        AntPathRequestMatcher.antMatcher("/balance"))
                                .access(new WebExpressionAuthorizationManager("hasRole('ADMIN') or hasRole('USER')"))

                                .requestMatchers(AntPathRequestMatcher.antMatcher(HttpMethod.GET, "/contact"),
                                        //so that errorDetails are added to the response
                                        AntPathRequestMatcher.antMatcher("/error"))
                                .permitAll()

                                .requestMatchers(
                                        AntPathRequestMatcher.antMatcher(HttpMethod.POST,"/contact"))
                                .hasRole("ADMIN")

                                //.requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll()
                                .requestMatchers(PathRequest.toH2Console()).permitAll()
                                .anyRequest().authenticated()
                ).requestCache((cache) -> cache
                        .requestCache(requestCache))
                .logout(
                        configurer -> configurer.logoutUrl("/logout").permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID")
                );

        http.addFilterAfter(new AuthoritiesLoggingAfterFilter(), BasicAuthenticationFilter.class);

        //We need to enable httpBasic when formLogin is enabled, so that REST apis can authenticate bearing a basic auth token
        http.formLogin(Customizer.withDefaults());
        http.httpBasic(Customizer.withDefaults());

        //Needed for h2-console frames
        http.headers(headers -> headers.frameOptions(frameOptionsConfig -> frameOptionsConfig.disable()));

        http.csrf(csrf -> csrf.disable());

        return http.build();
    }

    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}




